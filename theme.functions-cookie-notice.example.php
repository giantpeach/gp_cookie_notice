<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Example theme functions code for analytics
 */

// $run_analytics = apply_filters('gp_cookie_notice_google_analytics', false);
// if ($run_analytics) {
    // ANALYTICS CODE HERE
// }

add_filter('gp_cookie_notice_text', 'icc_cookie_notice_text');
add_filter('gp_cookie_notice_yes_button_text', 'icc_cookie_notice_yes_button_text');
add_filter('gp_cookie_notice_no_button_text', 'icc_cookie_notice_no_button_text');
// add_filter('gp_cookie_notice_yes_button_class', 'icc_cookie_notice_yes_button_class');
// add_filter('gp_cookie_notice_no_button_class', 'icc_cookie_notice_no_button_class');
add_filter('gp_cookie_notice_html', 'icc_cookie_notice_html', 20, 6);
// add_filter('gp_cookie_notice_reload_page', '__return_true', 10, 6);

function icc_cookie_notice_text($text) {
    $replace_text = get_field('cookie_notice_text', 'theme_settings');
    if (!empty($replace_text)) {
        return $replace_text;
    }
    return $text;
}

function icc_cookie_notice_yes_button_text($text) {
    $replace_text = get_field('cookie_notice_yes_button_text', 'theme_settings');
    if (!empty($replace_text)) {
        return $replace_text;
    }
    return $text;
}

function icc_cookie_notice_no_button_text($text) {
    $replace_text = get_field('cookie_notice_no_button_text', 'theme_settings');
    if (!empty($replace_text)) {
        return $replace_text;
    }
    return $text;
}

function icc_cookie_notice_html($html, $text, $yes_button_class, $yes_button_text, $no_button_class, $no_button_text) {
    return sprintf('<div class="cookie-notice"><div class="cookie-notice__inner"><div class="container">%s <div class="cookie-notice__buttons"><button class="cookie-notice__yes-button btn--small %s">%s</button><button class="cookie-notice__no-button btn--small %s">%s</button></div></div></div></div>', $text, $yes_button_class, $yes_button_text, $no_button_class, $no_button_text);
}