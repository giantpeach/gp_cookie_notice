# Giant Peach Cookie Notice - Wordpress Plugin

This plugin will add a cookie notice to the page.

When consent is given or not given, a cookie `gp_cookie_notice` is saved with the response either `accepted` or `rejected`

On giving consent, the plugin either loads `https://www.googletagmanager.com/gtag/js?id=UA-XXXXX-X` via JS and fires a page view, or, optionally, reloads the page (see below).

After that, the site checks for the existence of the cookie and if so, will allow analytics code to print.

**PLEASE NOTE: Editing the theme/other site files is also required to prevent the analytics code printing on the page initially (this depends on the analytics setup/how it is printed).**

### Example of code to add in theme (or elsewhere) to prevent analytics from running.

    $run_analytics = apply_filters('gp_cookie_notice_google_analytics', false);
    if ($run_analytics) {
        // DEFAULT ANALYTICS CODE HERE
    }

See theme.functions-cookie-notice.example.php for example of corresponding theme functions.

## Filters

`gp_cookie_notice_text` - The HTML (wysiwyg) copy of the cookie notice text. Use this filter to override the text via theme (based on an ACF field, for example).

`gp_cookie_notice_html` - The HTML of the cookie notice as a whole.

`gp_cookie_notice_yes_button_text` - The Yes button text.

`gp_cookie_notice_no_button_text` - The No button text.

`gp_cookie_notice_yes_button_class` - The Yes button class.

`gp_cookie_notice_yes_button_class` - The No button class.

`gp_cookie_notice_show` - Whether or not to show the cookie notice on the page. This will depend on whether the cookie is already saved. We also don't show the notice on the wp_privacy_policy page (if set).

`gp_cookie_notice_analytics_id` - The `UA-XXXXX-X` analytics ID.

`gp_cookie_notice_reload_page` - If the site analytics config is too complicated to fire something when consent is given, offer a fallback to just reload the page, so that everything we want to run will run once the consent cookie is set. Return true to enable this.

`gp_cookie_notice_is_notice_answered` - Could be overridden with this filter.
`gp_cookie_notice_is_notice_accepted` - Could be overridden with this filter.
`gp_cookie_notice_is_analytics_enabled` - Could be overridden with this filter.

## All in One SEO Pack

The plugin uses filter `aiosp_google_analytics` to prevent the code firing by default, and retrieves the AIOSEO Wordpress options to retrieve the saved analytics ID from the admin panel.

Integration for other plugins could be added in future versions.