<?php
/**
 * Plugin Name: Giant Peach Cookie Notice
 * Description: Adds functionality for Cookie Notice to consent to Analytics
 * Version: 1.0.5
 * Author: Giant Peach
 * Author URI: https://giantpeach.agency
 * Text Domain: gp_cookie_notice
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class GP_Cookie_Notice
{
    public function __construct() {
        add_action('wp_footer', [$this, 'print_analytics_id'], 10, 1);
        add_action('wp_footer', [$this, 'cookie_notice_html'], 10, 1);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_assets'], 10, 1);

        add_filter('gp_cookie_notice_show', [$this,'maybe_show_cookie_notice'], 10, 1);
        add_filter('gp_cookie_notice_analytics_id', [$this,'get_analytics_id'], 10, 1);

        // This filter can be used by other areas in theme etc to check whether to print the analytics code, true or false...
        // eg. $run_analytics = apply_filters('gp_cookie_notice_google_analytics', false);
        add_filter('gp_cookie_notice_google_analytics', [$this, 'check_if_analytics_enabled'], 10, 1);
        // Specific filter to prevent analytics for AIOSEO plugin
        add_filter('aiosp_google_analytics', [$this, 'aiosp_check_if_analytics_enabled'], 10, 1);

    }

    public function enqueue_assets()
    {
        wp_enqueue_style( 'cookie-notice-css', plugins_url( '/assets/dist/css/main.css', __FILE__ ), array(), '1.0.4' );
        wp_enqueue_script('cookie-notice-js', plugins_url( '/assets/dist/js/main.js', __FILE__ ), array(), '1.0.4', true );
    }

    public function get_analytics_id($analytics_id) {
        // Check if we have an All In One SEO plugin Analytics ID
        $aioseop_options = get_option( 'aioseop_options' );
        if ($aioseop_options && isset($aioseop_options['aiosp_google_analytics_id']) && !empty($aioseop_options['aiosp_google_analytics_id'])) {
            $analytics_id = $aioseop_options['aiosp_google_analytics_id'];
        }
        // TODO other plugin IDs
        return $analytics_id;
    }

    public function is_notice_answered() {
        // check if we have cookie set
        if ( isset($_COOKIE["gp_cookie_notice"]) && ($_COOKIE["gp_cookie_notice"] == 'accepted' || $_COOKIE["gp_cookie_notice"] == 'rejected') ) {
            return apply_filters('gp_cookie_notice_is_notice_answered', true);
        }
        return apply_filters('gp_cookie_notice_is_notice_answered', false);
    }

    public function is_notice_accepted() {
        // check if we have cookie set
        if (isset($_COOKIE["gp_cookie_notice"]) && $_COOKIE["gp_cookie_notice"] == 'accepted') {
            return apply_filters('gp_cookie_notice_is_notice_accepted', true);
        }
        return apply_filters('gp_cookie_notice_is_notice_accepted', false);
    }

    public function is_analytics_enabled() {
        // check if we have analytics enabled
        if ($this->is_notice_accepted()) {
            return apply_filters('gp_cookie_notice_is_analytics_enabled', true);
        }
        return apply_filters('gp_cookie_notice_is_analytics_enabled', false);
    }

    public function check_if_analytics_enabled($analytics) {
        if ($this->is_analytics_enabled()) {
            return true;
        }
        return false;
    }

    public function aiosp_check_if_analytics_enabled($analytics) {
        if ($this->is_analytics_enabled()) {
            return $analytics;
        }
        return false;
    }

    public function maybe_show_cookie_notice($show) {
        if (get_option( 'wp_page_for_privacy_policy' ) == get_the_ID()) {
            return false;
        }
        if ($this->is_notice_answered()) {
            return false;
        }
        return true;
    }

    /**
     * This variable is used by JS to fire ga if consent is accepted
     * (without needing another pageload)
     */
    public function print_analytics_id() {
        $analytics_id = apply_filters('gp_cookie_notice_analytics_id','');
        if (!empty($analytics_id) && !$this->is_analytics_enabled()) :
            // If the site analytics config is too complicated to fire something when consent is given
            // ... offer a fallback to just reload the page
            // ... so that everything we want to run will run when cookie is set
            $reload_page = apply_filters('gp_cookie_notice_reload_page',false);
            $reload_page = ($reload_page) ? 'true' : 'false';
        ?>
        <script>
            window.gp_cookie_notice_analytics_id = '<?php echo $analytics_id; ?>';
            window.gp_cookie_notice_reload_page = <?php echo $reload_page; ?>;
        </script>
        <?php
        endif;
    }

    /**
     * Prints cookie notice html in page footer
     */
    public function cookie_notice_html() {
        $show_cookie_notice = apply_filters('gp_cookie_notice_show', true);
        if (!$show_cookie_notice) {
            return;
        }
        $text = '<p>Do you give consent for us to use cookies to track your session on our website? This allows us to analyse our website\'s traffic in order to make improvements to the user experience.</p>';
        $text = apply_filters('gp_cookie_notice_text', $text);
        
        $yes_button_text = apply_filters('gp_cookie_notice_yes_button_text', 'Yes');
        $no_button_text = apply_filters('gp_cookie_notice_no_button_text', 'No');

        $yes_button_class = apply_filters('gp_cookie_notice_yes_button_class', 'btn button');
        $no_button_class = apply_filters('gp_cookie_notice_no_button_class', 'btn button');

        echo apply_filters('gp_cookie_notice_html',
            sprintf('<div class="cookie-notice"><div class="cookie-notice__inner"><div class="c container">%s </div><div class="cookie-notice__buttons"><button class="cookie-notice__button cookie-notice__yes-button %s">%s</button><button class="cookie-notice__button cookie-notice__no-button %s">%s</button></div></div></div>', $text, $yes_button_class, $yes_button_text, $no_button_class, $no_button_text),
            $text,
            $yes_button_class,
            $yes_button_text,
            $no_button_class,
            $no_button_text
        );
    }

}
new GP_Cookie_Notice();