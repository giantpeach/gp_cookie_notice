import Cookies from "js-cookie";

function CookieNotice() {

    var $this = this;

    this.is_visible = false;
    this.analytics_loaded = false;

    this.cookie_notice = document.querySelector('.cookie-notice');
    this.yes_button = document.querySelector('.cookie-notice__yes-button');
    this.no_button = document.querySelector('.cookie-notice__no-button');
    const isBot = !("onscroll" in window) || /glebot/.test(navigator.userAgent);

    // cookie notice affects crawling
    if (isBot) {
        return;
    }
    
    if (this.cookie_notice !== null && !this.getCookie()) {
        // if element exists and cookie not set, open it
        window.onload = function() {
            setTimeout(function(){ $this.open(); }, 1000);
        }
    }
    else {
        return;
    }
    
    if (this.yes_button !== null) {
        this.yes_button.addEventListener('click', function() {
            $this.setCookie('accepted');
            if (typeof window.gp_cookie_notice_reload_page !== 'undefined' && window.gp_cookie_notice_reload_page === true) {
                $this.reloadPage();
            }
            else {
                $this.fireAnalytics();
                $this.close();
            }
        });
    }
    
    if (this.no_button !== null) {
        this.no_button.addEventListener('click', function() {
            $this.setCookie('rejected');
            $this.close();
        });
    }
}

CookieNotice.prototype.setCookie = function(value) {
    Cookies.set('gp_cookie_notice', value, { expires: 30, path: '/' })
}

CookieNotice.prototype.getCookie = function(value) {
    return Cookies.get('gp_cookie_notice');
}

CookieNotice.prototype.open = function() {
    this.cookie_notice.classList.add('visible');
    this.is_visible = true;
}

CookieNotice.prototype.close = function() {
    this.cookie_notice.classList.remove('visible');
    this.is_visible = false;
}

CookieNotice.prototype.reloadPage = function() {
    location.reload();
}

CookieNotice.prototype.fireAnalytics = function() {

    var $this = this;
    if (this.analytics_loaded) {
        // duplicate click maybe
        return;
    }
    if (typeof window.gp_cookie_notice_analytics_id !== 'undefined')
    {
        const analytics_src = 'https://www.googletagmanager.com/gtag/js?id=' + window.gp_cookie_notice_analytics_id;
    
        let script = document.createElement('script');
        script.onload = function () {
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', window.gp_cookie_notice_analytics_id);
            $this.analytics_loaded = true;
        }
        script.src = analytics_src;
        document.head.appendChild(script);
    }
    
}

export default CookieNotice;
  
